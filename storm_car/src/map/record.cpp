/**
 * @file record.cpp
 * @author circleup (circleup@foxmial.com)
 * @brief GPS 数据录制节点
 * @version 0.1
 * @date 2020-07-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */


#include "serial_driver.h"
#include "global.h"

#include <math.h>

/**
 * @brief 观光车状态数据回调函数
 * 
 * @param msg serial_receive_data 话题消息
 */
void chatterCallback(const storm_car::car_status::ConstPtr& msg);

int main(int argc, char **argv)
{
  ros::init(argc, argv, "RECORD");

  ROS_INFO("This is RECORD");

  ros::NodeHandle n;

  ros::Subscriber receive = n.subscribe("serial_receive_data", 1000, chatterCallback);


  ros::spin();

  return 0;
}

void chatterCallback(const storm_car::car_status::ConstPtr& msg)
{


  ROS_INFO("record.heading is %f",msg->heading);
  ROS_INFO("record.lattitude is %f",msg->lattitude);
  ROS_INFO("record.longitude is %f",msg->longitude);
  ROS_INFO("record.velocity is %f",msg->velocity);
  
  WriteToFile(*msg);
}