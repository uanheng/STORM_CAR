/**
 * @file screen.cpp
 * @author circleup (circleup@foxmial.com)
 * @brief rviz 显示节点
 * @version 0.1
 * @date 2020-07-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "screen_driver.h"

#include "ros/ros.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "SCREEN");
  ros::NodeHandle n;

  ros::Rate loop_rate(0.5);

  while (ros::ok())
  {    
    loop_rate.sleep();
    ros::spinOnce();
  }

  return 0;
}
