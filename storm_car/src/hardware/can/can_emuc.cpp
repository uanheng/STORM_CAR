/**
 * @file can_emuc.cpp
 * @author circleup (circleup@foxmial.com)
 * @brief EMUC CAN 发送节点
 * @version 0.1
 * @date 2020-07-13
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#include "lib_emuc.h"

#include "ros/ros.h"
#include "storm_car/can_data.h"

/**
 * @brief 设置一帧数据
 * 
 * @param data 数据指针
 */
void set_emuc_data(DATA_INFO * data);

/**
 * @brief EMUC 数据发送函数
 * 
 * @param msg CAN_send_data 话题消息
 */
void SendROSDatatoCAN(const storm_car::can_data::ConstPtr& msg);

/**
 * @brief 工控机 CAN 数据发送节点
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv)
{
  ros::init(argc, argv, "CAN_EMUC");

  ROS_INFO("This is CAN_EMUC");

  ros::NodeHandle CAN;
  ros::Subscriber receive = CAN.subscribe("can_send_data", 1000, SendROSDatatoCAN);
  
  if (!EMUCOpenDevice(ttyACM0))
  {
    EMUCSetCAN(ttyACM0, EMUC_CH_1, EMUC_BAUDRATE_500K);
  }
  else
  {
    return 0;
  }
  
  ros::spin();
  return 0;
}

void SendROSDatatoCAN(const storm_car::can_data::ConstPtr& msg)
{
  DATA_INFO tmp;
  tmp.com_port = ttyACM0;
  tmp.channel = EMUC_CH_1;  // CAN 0：EMUC_CH_1，CAN 1：EMUC_CH_2
  tmp.mod = 0;              // 标准帧：0，远程帧：1
  tmp.rtr = 0;              // 数据帧：0，扩展帧：1
  tmp.dlc = 8;              // 数据长度 8

  tmp.id[0] =0x00;          // VCU 接受 ID：0x0238
  tmp.id[1] =0x00;
  tmp.id[2] =0x02;
  tmp.id[3] =0x38;

  for(int i = 0; i < 8; ++ i)
  {
    tmp.data[i] = msg->data[i];
  }
  EMUCSend(&tmp);
}