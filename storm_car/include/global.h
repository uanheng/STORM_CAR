/**
 * @file global.h
 * @author circleup (circleup@foxmail.com)
 * @brief 全局配置宏定义
 * @version 0.1
 * @date 2020-07-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

/**
 * @brief GPS数据存放位置（绝对路径）
 * 
 */
#define FILEPATH "/home/uanheng/workspace/carworkspace/STORMCAR/gps.txt"

/**
 * @brief PI 定义
 * 
 */
#define PI 3.14159265358979323846

/**
 * @brief 桌子高度
 * 
 */
#define HEIGHT 0.3

/**
 * @brief 车载导航雷达高度
 * 
 */
// #define HEIGHT 0.85

#endif