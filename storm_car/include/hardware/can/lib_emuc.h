/**
 * @file lib_emuc.h
 * @author EMUC
 * @brief EMUC CAN SDK 头文件
 * @version 0.1
 * @date 2020-07-13
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef __LIB_EMUC_H__
#define __LIB_EMUC_H__ 

#define MAX_COM_PORT    16
#define MAX_BUF         512

#define ID_LEN          4
#define DATA_LEN        8   
#define VER_LEN         16

#define DIM(var, type)  (sizeof(var)) / sizeof(type)

#define BOOL    bool
#define TRUE    true
#define FALSE   false

/**
 * @brief CAN 0 port
 * 
 */
#define ttyACM0 24

/**
 * @brief CAN 1 port
 * 
 */
#define ttyACM1 25

/**
 * @brief EMUC 端口配置枚举
 * 
 */
enum
{
  /* channel */
  EMUC_CH_1           = 1,
  EMUC_CH_2,
  EMUC_CH_BOTH,

  /* baudrate */
  EMUC_BAUDRATE_5K    = 0,
  EMUC_BAUDRATE_10K,
  EMUC_BAUDRATE_20K,
  EMUC_BAUDRATE_50K,
  EMUC_BAUDRATE_125K,
  EMUC_BAUDRATE_250K,
  EMUC_BAUDRATE_500K,
  EMUC_BAUDRATE_1M,
};

/**
 * @brief EMCU 数据发送格式
 * 
 */
typedef struct
{
  int           com_port;

  int           channel;
  int           mod;
  int           rtr;
  int           dlc;

  unsigned char id[ID_LEN];
  unsigned char data[DATA_LEN];

} DATA_INFO;

/**
 * @brief EMUC 硬件消息数据格式
 * 
 */
typedef struct
{
  char fw[VER_LEN];
  char api[VER_LEN];
} VER_INFO;

/**
 * @brief EMUC 打开设备
 * 
 */
extern "C" int  EMUCOpenDevice(int port);

/**
 * @brief EMUC 关闭设备
 * 
 */
extern "C" void EMUCCloseDevice(int port);

/**
 * @brief EMUC 读取硬件版本信息
 * 
 */
extern "C" int  EMUCShowVer(int port, VER_INFO *ver);

/**
 * @brief EMUC 设置 CAN 数据格式
 * 
 */
extern "C" int  EMUCSetCAN(int port, int ch, int bdrate);

/**
 * @brief EMUC 复位设备
 * 
 */
extern "C" int  EMUCReset(int port);

/**
 * @brief EMUC 接受 CAN 数据
 * 
 */
extern "C" int  EMUCReceive(DATA_INFO *info);

/**
 * @brief EMUC 发送 CAN 数据
 * 
 */
extern "C" int  EMUCSend(DATA_INFO *info);

/**
 * @brief EMUC 接受多帧 CAN 数据
 * 
 */
extern "C" int  EMUCReceiveNonblock(DATA_INFO *info, int cnt, unsigned int interval);

#endif
