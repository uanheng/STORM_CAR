/**
 * @file map_driver.h
 * @author circleup (circleup@foxmail.com)
 * @brief 地图运算加载驱动
 * @version 0.1
 * @date 2020-07-14
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef _MAP_DRIVER_H_
#define _MAP_DRIVER_H_

#include <vector>
#include <iostream>
#include <fstream>

/**
 * @brief UTM 坐标系 X Y
 * 
 */
typedef struct
{
  double E;
  double N;
}POS;

class map
{
private:
  /* data */
public:
  /**
   * @brief 规划点容器
   * 
   */
  std::vector<POS> g_points_plan;

  /**
   * @brief 当前点容器
   * 
   */
  std::vector<POS> g_points_current;

  map(/* args */);
  ~map();


  /**
   * @brief 一次性加载所有规划点(相对位置)
   * 
   * @return true 
   * @return false 
   */
  bool AddPlanPoint(void);

  /**
   * @brief GPS 坐标转 UTM 坐标系
   * 
   * @param longitude 经度
   * @param latitude 纬度
   * @param UTME UTM 坐标系 x 轴
   * @param UTMN UTM 坐标系 y 轴
   * @details GPS 坐标转 UTM 坐标系
   * 验证网页：http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html
   */
  static void LonLat2UTM(double longitude, double latitude, double& UTME, double& UTMN);

  void tranform_plan_to_car(class path);

};

#endif