/**@mainpage 自动寻迹观光车
* <table>
* <tr><th>Project  <td>STORM_CAR 
* <tr><th>Author   <td>circleup 
* <tr><th>Source   <td>~/workspace/STORMCAR/src
* </table>
* @section   项目详细描述
* -# 通过 CG610 传回的经纬度、偏航角、速度等信息来判断当前车的状态
* -# 通过当前状态以及事先的规划路径，实现寻迹功能，发送控制指令给下位机
* -# 通过下位机的 CAN 协议，发送控制指令，包括油门、档位、刹车、转向角度去控制车的移动
*
* @section   功能描述  
* -# 本工程基于 ROS（kinetic） 开发
* -# 使用 LCCAN 或工控机自身的 EMUCCAN 与下位机进行通讯，RS232 与 CGI610 通讯
* 
* @section   用法描述
* -# 把工程下的 stormcar.rviz 文件拷贝至 ~/.rviz 文件夹下；
* -# 使用 sudo apt install ros-kinetic-serial 安装串口库；
* -# 使用 catkin_make 编译代码；
* -# 使用 roslaunch storm_car ros_record.launch 启动记录模式，用于保存 gps.txt 文件；
* -# 使用 roslaunch storm_car ros_map.launch 启动仿真数据模式；
* -# 使用 roslaunch storm_car ros_rasterize.launch 启动点云聚类模式。
* 
* @section   程序更新 

* <table>
* <tr><th>Date        <th>H_Version <th>Author    <th>Description  </tr>

* <tr><td>2020-07-29  <td>0.2       <td>circleup  <td>
* -# 添加基于欧拉核的 K-Means++ 聚类算法。</tr>

* <tr><td>2020-07-27  <td>0.1       <td>circleup  <td>
* 创建初始版本
* -# 完成硬件驱动，实现基本指令控制，如转向、换挡、刹车；
* -# 使用PCL库实现滤波以及点云聚类（障碍物识别）；
* -# 实现位置记录功能。</tr>

* </table>

**********************************************************************************
*/