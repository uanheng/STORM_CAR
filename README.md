# STORM_CAR
自动巡迹观光车

## 项目详细描述
-  通过 CG610 传回的经纬度、偏航角、速度等信息来判断当前车的状态
-  通过当前状态以及事先的规划路径，实现寻迹功能，发送控制指令给下位机
-  通过下位机的 CAN 协议，发送控制指令，包括油门、档位、刹车、转向角度去控制车的移动

## 功能描述  
-  本工程基于 ROS（kinetic） 开发
-  使用 LCCAN 或工控机自身的 EMUCCAN 与下位机进行通讯，RS232 与 CGI610 通讯

## 用法描述
-  使用 sudo apt install ros-kinetic-serial 安装串口库；
-  使用 catkin_make 编译代码；
-  使用 roslaunch storm_car ros_record.launch 启动记录模式，用于保存 gps.txt 文件；
-  使用 roslaunch storm_car ros_map.launch 启动仿真数据模式；
-  使用 roslaunch storm_car ros_rasterize.launch 启动点云聚类模式。

## 硬件

OS      :   Ubuntu 16.04 x64

ROS     :   kinetic

USBCAN  :   EMUC-CAN & LCCAN

GPS     :   CGI610

LIDAR   :   RFans-16M
